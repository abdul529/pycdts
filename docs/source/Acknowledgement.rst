===================
Acknowledgement
===================
This material was based upon work supported by the
US Department of Energy's Office of Energy Efficiency and
Renewable Energy (EERE) under Solar Energy Technologies Office (SETO)
Agreement Number DE-EE0007536.

This work was the combined effort of 

* Arizona State University (ASU).

* First Solar Inc. (FSLR).

* San Jose State University (SJSU).

* Purdue University (Purdue).

The people involved in the Project are

* Abdul Rawoof Shaik (ASU).

* Igor Sankin (FSLR).

* Christian Ringhofer (ASU).

* Daniel Brinkman (ASU).

* Dmitry Krasikov (FSLR).

* Dragica Vasileska (ASU).

* Hao Kang (Purdue).

* Benes Bedrich (Purdue).

The **C# + Matlab** version of the tools is available online at <link after deployment>

The python code development and documentation was done by
**Abdul Rawoof Shaik** (ASU).
