============
Installation
============
The tool is available on gitlab at 
https://gitlab.com/abdul529/pycdts

The tool can be installed through PyPI with the following command
$ pip install pycdts

The tool documentation can be found at 
https://pycdts.readthedocs.io/en/latest/

Installation is recommended through pip and any issue with the installation and 
running the tool can be directed to arshaik@asu.edu

To check if the installation is done correctly please type the following in a python command window

>>> import pycdts

If there are no errors then the installation is good.

.. run_instructions:

To run the tool type cdts.py in the command window. If it fails then use the following commands in a python-command window

>>> from pycdts.PVRD import PVRD
>>> ex=PVRD()
>>> ex.startApplication()

The above should open the tool window as shown below

.. image:: ./images/welcomeImg.JPG