=========================
Getting Started Tutorial
=========================
In this tutorial we show how to create a simple 0D project along with familiarizing the 
tool interface.

After starting the tool following the run_instructions_ we get a window with welcome page.

.. image:: ./images/welcomeImg_1.JPG

The window contains a menu bar at top with file menu and help menu, status bar at bottom.
The file menu consists of
* New action (for creating new project)
* Open action (for opening previously saved projects)
* Save action (to save the project to a file)
* Save As action (to Save the project to a different file)

The keyboard shortcuts are as displayed.

Click on New... action or press Ctrl+N for creating a new project. This opens the project
geometry type dialog box asking user to select the geometry dimension of the simulation 
structure of the project. 

.. image:: ./images/welcomeImg_2.JPG

Select the 0D button and click OK to create a new 0D project.
This opens Project editor window as shown

.. image:: ./images/welcomeImg_3.JPG

The editor window can be maximized. This window has DB, Geo DCS, DCMS, Sim, Mesh and Run
at the top. The DB is active and others are disabled. 

Click the DB action menu to select the database to be used in the project. Browse through
to the location containing the database. For this tutorial use the database provided with 
the source files from gitlab.com or download the database file directly from 
https://gitlab.com/abdul529/pycdts/blob/master/Database/test.db to a folder and select
it from the navigation window.

.. image:: ./images/Proj_mode_1.JPG

If the action is successful it shows the message 'loaded the database' in the status bar 
and activates the Geo action menu.

Click on the Geo action menu and it opens the geometry editing window. 

.. image:: ./images/Proj_mode_2.JPG

Click on the Dot (0D) button on the right end panel to open a structure creating dialog box.
Since our project is 0D there is nothing to select/modify in the dialog box.
Click OK in the dialog box.

After that save button gets activated in the right end panel. Click on it to save the project
to a file. Saving is necessary from this step onwards.
Upon clicking the save button (or Ctrl+s) it opens a file save dialog box. Save the project with
name as 'test0D'. 

.. image:: ./images/Proj_mode_2_1.JPG

After the project is saved the materials tab gets activated. Click on the Materials tab to specify
the materials for the geometric structure. On the right hand panel there are buttons listing the 
materials present in the database we selected during the DB operation. In the test.db file there are
4 materials namely CdS, CdTe, ZnTe and SnO. Click on the CdTe button to open a dialog box to create 
CdTe material layer. Since our project is 0D nothing can be selected/modified. Click OK to add CdTe
material for the 0D point.

.. image:: ./images/Proj_mode_2_2.JPG
 
Once we add CdTe material, save button gets enabled and first save the structure and then click on 
done button in the right hand panel.

After clicking done button Grain Boundaries tab gets enabled. Click on Grain Boundaries tab to setup
the grain boundaries. In 0D nothing needs to be done in this step. Click on Done button on the right
hand panel. This enables DCS action in the top bar of the project editor window.

.. image:: ./images/Proj_mode_2_3.JPG

Clicking on the DCS action button shows structure window and a right hand panel displaying Mechanisms
Reactions, Species and a Done button.

.. image:: ./images/Proj_mode_3.JPG

The mechanism list provides a list of mechanisms defined in the database. Similarly the reaction list
and species list provides a list of reactions and species (charge carriers as well as point defects) 
available in the database.

The structure window shows the created structure from the Geo action mode.
The structure window can be scaled or fitted to the screen through mouse middle scroll button (for scaling)
and for fitting press 'f' key. Press f and select the rectangle displayed.
Then, click on the dropdown menu of Reaction list and select 'Null <=> e_{c}^{-} + h_{v}^{+}' reaction. Hold the
left mouse key and drag and drop into the structure window on top of the 0D rectangle box.

.. image:: ./images/Proj_mode_3_1.gif

.. image:: ./images/Proj_mode_3_1a.JPG
   
To see if the reaction is currently add to the selected structure, please select the structure where it
is added (here it is the 0D rectangle box) and press 'q' or double click on it. It opens a query dialog box
as shown below

.. image:: ./images/Proj_mode_3_1b.JPG

In the query dialog box, see the Reaction(Text): field text box and confirm that the reaction added is present.
In the PointDefects(Text): field text box the point defect species involved in the reaction will be automatically
added. Press OK or cancel to close the window. To delete the added reaction select the reaction text in the text box and
delete the corresponding text. For example, let us say I accidentally added the reaction 'A_{A}^{0} <=> A_{A}^{-} + h_{v}^{+}'
and would like to delete it. I would select the text and delete it (see figure below).

.. image:: ./images/Proj_mode_3_1c.JPG

For the tutorial purpose make sure that you add the following reactions: 
(1) 'NULL <=> e_{c}^{-} + h_{v}^{+}',
(2) 'D_{C}^{0} <=> D_{C}^{+} + e_{c}^{-}',
(3) 'D_{C}^{+} <=> D_{C}^{0} + h_{v}^{+}',
(4) 'A_{A}^{0} <=> A_{A}^{-} + h_{v}^{+}',
(5) 'A_{A}^{-} <=> A_{A}^{0} + e_{c}^{-}'

After adding all the reaction click on done button on the right side panel which activates the DCMS action button on the top.
Click on the DCMS action button to specify the model parameters for the defect chemical reactions and point defect species.

.. image:: ./images/Proj_mode_3_2.JPG

After clicking the DCMS action button the structure window shows a dot. Again press key 'f' to fit to screen and double click 
the 0D rectangle box (or press key 'q' after selecting it) to open dialog window as shown below. In the dialog box click on the 
properties button next to the Material: CdTe text box as shown below.

.. image:: ./images/Proj_mode_4_1.JPG

On clicking it opens a new dialog box window displaying the material model parameters for the CdTe material set for the simulation.
By default the models and model paramters are taken for the database selected in DB action button step. In this dialog box, we can
edit the material model parameters (namely effective masses, lattice density, electron affinities, dielectric constants, band gap model params etc)
only applicable to the current simulation. The user can reset the values to those from the database simply by clicking the reset button 
in the bottom. 

In a similar way we can edit the model properties of reactions and species.

.. image:: ./images/Proj_mode_4_2.JPG
.. image:: ./images/Proj_mode_4_3.JPG

For this tutorial purpose please do not change any model parameters. (If you change accidentally you can always reset it). You can save your changes up 
until this step. The save functionality might not be working in the forthcoming steps.
We note that the models cannot be changed from the default models from the database in this version (0.1). This will be changed in future release versions. 
Please note that this step might take time to process. This mainly because of the GUI coding limitation. This will be improved 
in future version of the tool.

After the defect chemistry model specification (DCMS) step press the button done on the right panel and it enables the Sim action button. Click on the 
Sim action button to specify the simulation conditions for the run. The simulation conditions are mainly the initial conditions and boundary conditions,
temperature profile in time, bias profile in time and light illumination profile in time. To specify the simulation initial condition click on the structure
and double click on the 0D rectangle box to open up the simulation initial condition dialog box as shown below

.. image:: ./images/Proj_mode_5_1.JPG

In the dialog box set the initial condition of the species 'D_{C}^{0}' as 1e17 and 'A_{A}^{0}' as 1e13 for the tutorial purposes and click ok to close the dialog box.
Click on the Temperature button present in the bottom to specify the temperature profile in time. For this tutorial leave the temperature profile as it is.
Click on the Light button present next to the temperature profile button. Just inspect it and leave as it is. Do the same thing for Bias as well. Since 
our simulation is a 0D bias does not do anything. After setting up press the done button.

This enables the Mesh action button. Click on the Mesh action button to see the mesh editor. Since we are doing a 0D simulation meshing can be skipped.

.. image:: ./images/Proj_mode_6_1.JPG

Press the done button on the right panel. This enables the Run action button on the top. Press it to specify the run paramters namely tolerances and other numerical algorithm 
parameters.

.. image:: ./images/Proj_mode_7_1.JPG

In the buttom press the run button to start running the simulation. If it pops up any dialog boxes just click ok. After the simulation is done, it opens up solution browser
window as shown.

.. image:: ./images/Proj_mode_8_1.JPG

The solution browser window shows how the concentration of the defect species evolve over time.
In this tutorial we set up a generic donor and generic acceptor reactions as well as thermal generation recombination reactions in CdTe.
We specified initial conditions of 1e17 generic neutral donor and 1e13 generic neutral acceptor. These participate in the ionization reactions 
and finally gives free carriers. Since we have donor concentration far greater than acceptor concentration we see at the end of simulation time (10 sec)
an electron concentration of 3.2e16 (overlapped with D_{C}^{+} concentration) and hole concentration of 2.8e-5.

This completes the first tutorial.