=====
About
=====

.. raw:: html

    <script type="text/javascript" >
    MathJax.Hub.Config({
        TeX: { extensions: ["mhchem.js"] }
    });
    </script>

Introduction
------------
 There are many software tools available to understand and study the transport of charge carriers
 namely electrons and hole in materials. Very few tools exist to study the transport of 
 point defects in materials. Moreover, the chemistry of point defects are addressed in almost
 no tool. 
 
 In our tool we developed a novel approach to study the transport of charge carriers and point 
 defects on an equal footing along with the incorporation of point defect chemistry.
 
Theory
------
 The equation used for describing the transport of charge carriers are 
 
 .. math:: \frac{\partial n}{\partial t}=-\nabla \cdot \overrightarrow{J_n} + G_n - R_n 
	:label: eq1
 .. math:: \overrightarrow{J_n}=-\mu_n n \overrightarrow{E}-D_n \nabla n 
    :label: eq2
 .. math:: \frac{\partial p}{\partial t}=-\nabla \cdot \overrightarrow{J_p} + G_p - R_n 
    :label: eq3
 .. math:: \overrightarrow{J_p}=\mu_p p \overrightarrow{E} - D_p\nabla p 
    :label: eq4
 .. math:: \nabla\cdot\left(\varepsilon \nabla \phi\right)=-p+n-N_D+N_A
    :label: eq5

 where :math:`\overrightarrow{J_n}`, :math:`\overrightarrow{J_p}` are the flux densities instead
 of the usual current densities used in semiconductors, :math:`\overrightarrow{E}` is the electric
 field, :math:`\phi` is the electrostatic potential, :math:`G` and :math:`R` are the generation and
 recombination rates modeled for radiative, SRH, Auger etc., :math:`N_D` and :math:`N_A` are the net
 donor and acceptor concentrations. For heterostructures, an additional gradient term should be added
 to the drift-diffusion equation :eq:`eq2` and :eq:`eq4`. The transport of point defects also follow
 the similar equations.
 
 We can arrive at the same equations by treating diffusion flux as negative gradient of electro
 chemcial potential (:math:`\mu`) defined as
 
 .. math:: \mu=G_f^0 + q\phi+kT\ln\left(\frac{u}{N_s}\right)
    :label: eq6
 
 where :math:`G_f^0` is the formation energy of the species, :math:`\phi` is the electrostatic potential,
 :math:`q` is the charge of the species, :math:`T` is the temperature, :math:`k` is the Boltzmann constant,
 :math:`u` is the concentration of the species and :math:`N_s` is the maximum number of microstates the 
 species can occupy. The diffusion flux is given as
 
 .. math:: \overrightarrow{J}=-\frac{D}{kT}u\nabla\mu.
    :label: eq7
 
 Please note that we used Einstein relationship between mobility and diffusivity in writing :eq:`eq6` and
 :eq:`eq7`. Thus the transport equation are given as
 
 .. math:: \frac{\partial u_i}{\partial t}=-\nabla \cdot \overrightarrow{J_i} + G_i - R_i
    :label: eq8
 .. math:: \overrightarrow{J_i}=-\frac{D}{kT}u_i\nabla\left(G_{f,i}^0 + q_0 z_i\phi + kT\ln\left(\frac{u_i}{N_{s,i}}\right)\right)
    :label: eq9
 .. math:: -\nabla\cdot\left(\varepsilon \nabla \phi \right)=q_0\sum_i z_iu_i
    :label: eq10
	
 where :math:`i` is the index of the species, :math:`q_0` is the charge of an electron, :math:`z_i` stands for
 the ionization number of the charge state with sign. The above equations can be applied to both charge carriers
 as well as point defects. The :eq:`eq8` requires the computation of generation and recombination rates 
 :math:`G_i-R_i`. For charge carries these rates are well known and can be computed with algebraic expressions.
 For example if the charge carriers involve in radiative recombination then the :math:`G_i-R_i` is given as
 
 .. math:: \left(G_i-R_i\right)|_{radiative}=B\left(n_i^2-np\right)
    :label: eq10_1
	
 For trap assisted recombination (SRH) we have
 
 .. math:: \left(G_i-R_i\right)|_{SRH}=\frac{n_i^2 - np}{\frac{n+n_1}{C_p}+\frac{p+p_1}{C_n}}
    :label: eq10_2

 where
 
 .. math:: n_1=N_c e^{-\frac{E_C-E_T}{kT}}
    :label: eq10_3
 .. math:: p_1=N_v e^{\frac{E_V-E_T}{kT}}
    :label: eq10_4
	
 Here :math:`N_C`, :math:`N_V` are conduction and valence band density of states, :math:`E_C`, :math:`E_V` and 
 :math:`E_T` are conduction band, valence band and trap energy levels respectively.
 
 For point defects these rate are generally not known. We employ defect chemical reaction formalism and use 
 chemical reaction kinetics to compute the generation and recombination rate for point defects. Considering charge
 carries as a point defect species we can show that generation and recombination rates obtained from reaction kinetics
 are equivalent to the general expressions given in :eq:`eq10_1` and :eq:`eq10_2`.
 
 The interactions between point defects (including charge carriers) can be represented as defect chemcial reaction.
 Considering a general reaction
 
 .. math:: \ce{aA + bB <=>[K_f][K_b] cC + dD}
    :label: eq11
 
 The rate equation can be written as
 
 .. math:: -\frac{1}{a}\frac{d[A]}{dt}=-\frac{1}{b}\frac{d[B]}{dt}=\frac{1}{c}\frac{d[C]}{dt}==\frac{1}{d}\frac{d[D]}{dt}=K_f[A]^a[B]^b - K_b[C]^c[D]^d
    :label: eq12
	
 In our tool we consider only point defect chemical reaction with atmost 2 reactants or atmost 2 products with stoichiometric 
 coefficients a,b,c and d to be either 1 or 0. For such point defect reactions we show our method of computing the generation
 recombination rates. If any of the species is involved in more than one reaction then the rate equation for that species have
 contributions from all the invovled reactions.
 
 Consider the following reaction system which represent almost all reactions considered in our tool.
 
 .. math:: \ce{A + B <=>[K_{f1}][K_{b1}]C + D}
    :label: eq13
 .. math:: \ce{A <=>[K_{f2}][K_{b2}]E + F}
    :label: eq14
 .. math:: \ce{Null <=>[K_{f3}][K_{b3}]A + G}
    :label: eq15
 
 Here species A is invovled in the 3 reactions as shown. Thus for this we write the rate equation as
 
 .. math:: \frac{d[A]}{dt}=-K_{f1}[A][B]+K_{b1}[C][D]-K_{f2}[A]+K_{b2}[E][F]+K_{f3}-K_{b3}[A][G]
    :label: eq16
	
 Once this rate equations are formed we systematically rewrite them as
 
 .. math:: \frac{d[A]}{dt}=U^TQ_AU+P_AU+K_A
    :label: eq17
	
 where :math:`U` is a concentration vector (column) of length 7 (total number of species in the system),
 
 .. math:: U=\left([A],[B],[C],[D],[E],[F],[G]\right)^T
    :label: eq18
 
 :math:`Q_A` is a 7X7 matrix with all zero elements except for
 
 .. math:: q_{12}^A=q_{21}^A=-\frac{K_{f1}}{2},q_{34}^A=q_{43}^A=\frac{K_{b1}}{2},q_{56}^A=q_{65}^A=\frac{K_{b2}}{2},q_{17}^A=q_{71}^A=-\frac{K_{b3}}{2}
    :label: eq19
	
 :math:`P_A` is a row vector of length 7 with all elements zero except
 
 .. math:: p_1^A=-K_{f2}
    :label: eq20
	
 and :math:`K_A` is the constant term given as :math:`K_{f3}`.
 
 The above representation of rates as :eq:`eq17` can be extended to any species and we can write the corresponding :math:`Q`, :math:`P`, :math:`K` variables
 as functions of reaction rate constants. For a general reaction system with M species, the rate equation for the :math:`i` species is given as
 
 .. math:: \frac{du_i}{dt}=\sum_{k=1}^{M}\sum_{j=1}^{M}a_{jk}^i u_ju_k+\sum_{j=1}^{M}b_j^iu_j+c^i`
    :label: eq21
	
 for :math:`i=1,2,...,M`, and a,b,c are the reaction rate constants in which the :math:`i` species is involved.
 
 Then the variables :math:`Q^i, P^i, K^i` can be calculated as
 
 .. math:: q_{jk}^i=q_{kj}^i=\left\{\begin{array}{l l} \frac{1}{2} a_{jk}^i, &\text{if } j\neq k \\ a_{jk}^i, &\text{if} j=k\\ 0, &\text{otherwise} \end{array} \right.
    :label: eq22
 .. math:: p_j^i=b_j^i, K=c^i.
    :label: eq23
	
 Thus, the generation recombination expression for any point defect species (including free carriers) can be expressed as
 
 .. math:: G_i-R_i = \frac{du_i}{dt}\Bigg|_{reactions}=U^T Q^i U + P^iU+K^i
    :label: eq24
	
 The net rate as reaction operator :math:`R(U)` can be written as
 
 .. math:: R(U)=U^TQU+PU+K
    :label: eq25
	
 where :math:`Q` is MxMxM array, :math:`P` is MxM array and :math:`K` is Mx1 array.
 
 The major advantage of writing :math:`R(U)` in the above form is that we can calculate the Jacobian of the reaction operator with the formula
 
 .. math:: J_R(U)=(Q+Q^T)U+P
    :label: eq26
	
 We thus showed you how we calculate the generation reaction term for point defects using the point defect chemical reaction formalism.
 
Equivalence
"""""""""""
 In this section we show you the equivalence of writing radiative process and trap assisted process (SRH) as defect chemical reaction. The 
 radiative generation and recombination rate and SRH rates are given as :eq:`eq11` and :eq:`eq12` respectively.

 We represent radiative process as

 .. math:: \ce{Null <=>[K_f][K_b] $e_c^-$ + $h_v^+$}
    :label: eq28

 where :math:`e_c^-` and :math:`h_v^+` denote the free electron and hole in conduction and valence band of the material. Hence the rate 
 equation is given as

 .. math:: \frac{d[e_c^-]}{dt}=\frac{d[h_v^+]}{dt}=K-K_b[e_c^-][h_v^+]=K_b\left(\frac{K_f}{K_b}-[e_c^-][h_v^+]\right)
    :label: eq29

 Thus, if the ratio of forward to backward rate constant is :math:`n_i^2` we can easily see that :math:`eq11` and :math:`eq29` are equivalent.
 The thermodynamics of reactants and products results in such condition for the ratio. This will be shown in next section.

 The equivalent reaction representing the SRH process in case of acceptor type specis can be written as

 .. math:: \ce{A^0 <=>[K_{f1}][K_{b1}] A^- + $h_v^+$}, \ce{A^- <=>[K_{f2}][K_{b2}] A^0 + $e_c^-$}
    :label: eq31

 The rate equations can be written as

 .. math:: \frac{d[A^0]}{dt}=-\frac{d[A^-]}{dt}=-K_{f1}[A^0] + K_{b1}[A^-][h_v^+] + K_{f2}[A^-]-K_{b2}[A^0][e_c^-],
    :label: eq32
 .. math:: \frac{d[e_c^-]}{dt}=K_{f2}[A^-]-K_{b2}[A^0][e_c^-],
    :label: eq33
 .. math:: \frac{d[h_v^+]}{dt}=K_{f1}[A^0]-K_{b1}[A^-][h_v^+].
    :label: eq34

 From :eq:`eq32` we note that :math:`[A^0]+[A^-]` is constant in time (say D). Hence in steady state equilibrium for :math:`[A^0]` and :math:`[A^-]` we have

 .. math:: [A^0]=\frac{K_{f2}+K_{b1}p}{K_{f1}+K_{f2}+K_{b1}p+K_{b2}n}D , [A^0]=\frac{K_{f1}+K_{b2}n}{K_{f1}+K_{f2}+K_{b1}p+K_{b2}n}D
    :label: eq35

 where :math:`[h_v^+]=p` and :math:`[e_c^-] = n` is used for notational simplicity.
 
 Using :eq:`eq35` in :eq:`eq33` and :eq:`eq34` with few algebraic simplifications we can write

 .. math:: \frac{dn}{dt}=\frac{dp}{dt}=\frac{K_{f1}K_{f2}-K_{b1}K_{b2}np}{K_{f1}+K_{f2}+K_{b1}p+K_{b2}n}
    :label: eq36

 Now if the ratio of the reaction rates is restricted as

 .. math:: \frac{K_{f1}}{K_{b1}}=p_1=N_V e^{\frac{E_V-E_T}{kT}} \frac{K_{f2}}{K_{b2}}=n_1=N_c e^{-\frac{E_C-E_T}{kT}}
    :label: eq37

 then :eq:`eq36` can be simplified further as

 .. math:: G-R=\frac{n_i^2-np}{\frac{n+n_1}{K_{b1}}+\frac{p+p_1}{K_{b2}}}D
    :label: eq38

 which is equivalent to :eq:`eq12`. The thermodynamics of reactants and products will result in :eq:`eq37` which will be shown in the next section. A similar 
 equivalence can be shown for donor type centers as well.

Thermodynamic Relationship between Forward and Backward Rate Constants
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 Consider a general reaction

 .. math:: \ce{$\sum_{i=1}^{V}$ $x_i X_i$ <=>[K_f][K_b] $\sum_{j=1}^{M}$ $y_j Y_j$}
    :label: eq39

 with N reactants and M products. At thermodynamic equilibrium the difference in the chemical potential of reactants and products should be zero. Thus, we have

 .. math:: \Delta_r G=\sum_{i}^N x_i \mu_{X_i}-\sum_{j=1}^{M} y_j \mu_{Y_j}
    :label: eq40

 where :math:`\Delta_r G` is the Gibb's free energy change of reaction, :math:`\mu` is the chemical potential of the species which can be written from :eq:`eq6` neglecting
 the electrostatic potential as

 .. math:: \mu_{X_i} = G_{f,X_i}^0 + kT\ln\left(\frac{[X_i]}{N_{s,X_i}}\right)
    :label: eq41

 Thus, at equilibrium we have

 .. math:: \sum_{i=1}^N\left( x_iG_{f,X_i}^0 + kT\ln\left(\frac{[X_i]_{eq}^{x_i}}{N_{s,X_i}^{x_i}}\right) \right)=\sum_{j=1}^M\left( y_jG_{f,Y_j}^0 + kT\ln\left(\frac{[Y_j]_{eq}^{y_j}}{N_{s,Y_j}^{y_j}}\right) \right)
    :label: eq42

 Upon simplification we get

 .. math:: \frac{\prod_{j=1}^M [Y_j]_{eq}^{y_j}}{\prod_{i=1}^N [X_i]_{eq}^{x_i}}=\frac{\prod_{j=1}^M N_{s,Y_j}^{y_j}}{\prod_{i=1}^N N_{s,X_i}^{x_i}}e^{-\frac{\Delta_r G^0}{kT}}
    :label: eq43

 where :math:`\Delta_r G^0` is the standard enthalpy change of reaction or standard reaction Gibb's energy defined as

 .. math:: \Delta_r G^0 = \sum_{j=1}^M y_j G_{f,Y_j}^0 - \sum_{i=1}^M x_i G_{f,X_i}^0
    :label: eq44

 We also know that at equilibrium we have

 .. math:: K_{f}\prod_{i=1}^N[X_i]^{x_i}{eq}-K_b\prod_{j=1}^M[Y_j]_{eq}^{y_j}=0
    :label: eq45

 Hence using :eq:`eq43` and :eq:`eq45` we can write

 .. math:: \frac{K_f}{K_b}=\frac{\prod_{j=1}^M N_{s,Y_j}^{y_j}}{\prod_{i=1}^N N_{s,X_i}^{x_i}}e^{-\frac{\Delta_r G^0}{kT}}
    :label: eq46

 Therefore, the reaction rate constants should have a fixed ration. Applying this to the reaction :eq:`eq28` we have

 .. math:: \frac{K_f}{K_b}=N_{s,e_c^-}N_{s,h_v^+}\exp\left(-\frac{G_{f,e_c^-}^0G_{f,h_v^+}^0}{kT}\right)
    :label: eq47

 The number of microstates for electrons in conduction band and hole is valence band are :math:`N_C` and :math:`N_V` respectively. The formation energies of electrons in conduciton band and holes in
 valence band referenced from vacuum level is

 .. math:: G_{f,e_c^-}^0 = -\chi, G_{f,h_v^+}^0=\chi+E_g
    :label: eq48

 where :math:`\chi` is the electron affinity of the material and :math:`E_g` is the band gap of the material. Thus, we have the ratio as

 .. math:: \frac{K_f}{K_b}=N_C N_V \exp\left(-\frac{E_g}{kT}\right)
    :label: eq49

 Considering the reactions in :eq:`eq31` we have

 .. math:: \frac{K_{f1}}{K_{b1}}=\frac{N_{s,A^-}N_{s,h_v^+}}{N_{s,A^0}}\exp\left( -\frac{G_{f,A^-}^0+G_{f,h_v^+}^0-G_{f,A^0}^0}{kT} \right)
    :label: eq50
 .. math:: \frac{K_{f2}}{K_{b2}}=\frac{N_{s,A^0}N_{s,e_c^-}}{N_{s,A^-}}\exp\left( -\frac{G_{f,A^0}^0+G_{f,e_c^-}^0-G_{f,A^-}^0}{kT} \right)
    :label: eq51

 where the formation energies are given as

 .. math:: G_{f,A^0}^0 = E_{f,A^0} - \chi - E_g, \\ G_{f,A^-}^0 = E_{f,A^0} - sign(A^-)E_T - \chi - E_g\\ G_{f,e_c^-}^0=-\chi, G_{f,h^+_v}^0 = \chi+E_g
    :label: eq52

 Here :math:`sign(A^-)` is the sign of the charge on the species, :math:`E_T` is the trap level (transition level from 0/-) with respect to vacuum and 
 :math:`E_{f,A^0}` is the defect formation energy of neutral species calculated through Density Functional Theory (DFT) referenced with respect to valence band.
 Assuming that the microstates for neutral and charged species are nearly same, we get

 .. math:: \frac{K_{f1}}{K_{b1}}=N_V \exp\left( -\frac{\chi+E_g+E_T}{kT} \right)=N_V\exp\left( \frac{E_V-E_T}{kT} \right) \\ \frac{K_{f2}}{K_{b2}}=N_C \exp\left( -\frac{-\chi-E_T}{kT} \right) = N_C\exp\left( -\frac{E_C-E_T}{kT} \right)
    :label: eq53

 where we have used the relations

 .. math:: E_C=-\chi, E_V=-\chi-E_g
    :label: eq54

 Hence, for SRH process, the thermodynamics guarantees the equivalence as a chemical defect reaction.

Models
------
 We have presented the theory behind our approach and the differential equations involved in the transport of charge carries and point defects. In this section we give
 the models we have employed in our solver.

Reaction Models
"""""""""""""""
  Although thermodynamics provides the ratio of forward and backward rate constants, we still have to find either one of the rate constants. For this we use reaction models
  that provide the rate constant of the reaction either forward rate or backward rate. Consider a general reaction 

  .. math:: \ce{ A^{z_A} + B^{z_B} ->[K] C^{z_C} + dD^{z_D}}
     :label: eq55

  where :math:`d` can be 0 or 1 with 0 being single product reaction and 1 being two product reaction, :math:`z_X` is the ionization of species X. The diffusion-controlled reaction
  rate constant for the above reaction is given as

  .. math:: K=4\pi R_{capt} (D_A+D_B) \exp\left( -\frac{E_A}{kT} \right)
     :label: eq56

  where :math:`R_{capt}` is the capture radius, :math:`D_X` is the diffusivity of species X, :math:`E_A` is the activiation energy of reaction ( :math:`E_A \ge 0`) representing the 
  probability of a collision resulting in the formation of product.

  The various types of reaction models used in the solver are as given below

  * Diffusion Limited with Attraction Model:

  .. math:: K = q\frac{|z_A z_B|}{\varepsilon kT}(D_A+D_B) \text{ where model is applicable only if } z_A\times z_B <0
     :label: eq57

  * Capture Radius Limited Model:

  .. math:: K = 4\pi R_{capt}(D_A+D_B) \text{ where model is applicable only if } z_A \times z_B \ge 0
     :label: eq58

  * Capture Cross Section Limited Model:

  .. math:: K = \sigma_{e/h} v_{th,e/h} \text{ where model is applicable whenever one reactant }\\ \text{ is either electron or hole and only if} z_A \times z_B \ge 0
     :label: eq59

  * Thermal Generation-Recombination Model:

  .. math:: K = B_0 \text{ where model is applicable only when the reactants are both electron} \\ \text{ and hole. Here } B_0 \text{ is the band to band recombination rate}\\ \text{ coefficient of the material}
     :label: eq59_1

  * Barrier Limited Model:

  .. math:: K=\nu \exp\left( -\frac{E_b}{kT} \right) \text{ where model is applicable with sigle reactant and single product} \\ \text{Here } \nu \text{ is the attempted frequency}\\ E_b \text{ is the reaction activation barrier}
     :label: eq63

Diffusion Models
""""""""""""""""
  The diffusion coefficient of species is assumed to have Arrhenius relationship with temperature given as

  .. math:: D(T) = D_0 \exp\left( -\frac{E_D}{kT} \right)

  The diffusion coefficient of free carriers are assumed to follow Einstein relationship given as

  .. math:: D_{e/h} = \mu_{e/h}\frac{kT}{q}

  The mobility of free carriers are assumed to follow the relationship

  .. math:: \mu_{e/h} = \mu_{e/h,300K} \left(\frac{T}{300}\right)^{-\frac{3}{2}}

Band Gap Models
"""""""""""""""
  We use Varshni model for the temperature dependence of band gap given as

  .. math:: E_g(T) = E_0 - \alpha \left(\frac{ T^2}{ T+ \beta}\right)

  where :math:`E_0` is the band gap at 0K, :math:`\alpha` and :math:`\beta` are fitting parameters.

Density of States Models
""""""""""""""""""""""""
  For charge carriers the density of states are given as

  .. math:: 2 \left( \frac{2\pi m_{eff,e/h} m_0 kT}{h^2} \right)^{\frac{3}{2}}

  For defect species the density of microstate are given as

  .. math:: N_s = g_{atom}\times g_{elec} \times N_0

  where :math:`g_{atom}`, :math:`g_{elec}` are the atomic and electronic degeneracy factors, :math:`N_0` is the lattice site density of the host material.
  The sites available to the point defects in the host material depends on the host crystal structure, size of the point defect etc. Hexagonal site, tetrahedral site etc are  
  examples of lattice sites in zincblende crystal structure.

Formation Energy of Species Models
""""""""""""""""""""""""""""""""""
  For charge carriers the formation energy is given as

  .. math:: \Delta H_f(e)= -\chi, \Delta H_f(h) = \chi+E_g(T)

  For point defect species the formation energy is

  .. math:: \Delta H_f(X^{z_x})=\Delta H_f(X^0) - sign(z_x)*\left( \frac{T}{300}\Delta E_T -\chi -E_g(T) \right)

This completes the description of the solver.
