.. PVRD_test documentation master file, created by
   sphinx-quickstart on Wed Feb 13 12:14:39 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PVRD_test's documentation!
=====================================
This is a test documentation for PVRD tool. The PVRD tool is useful for 
studying defect chemistry based transport of atomic species(including 
electrons and holes) under various simulation conditions

Contents
========

.. toctree::
   :maxdepth: 2
   
   About
   Installation
   Tutorials
   NumericalAlgorithms
   Modules
   Contact
   Development
   Acknowledgement
   Frequently Asked Questions


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

