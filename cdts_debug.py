#! /usr/bin/env python
from pycdts.PVRD import PVRD
from PyQt5.QtCore import pyqtRemoveInputHook
import sys, os

pyqtRemoveInputHook()
sys.path.append(os.path.join(os.getcwd()))

ex=PVRD()
ex.startApplication()
