PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE INTERFACEREACTIONS (material,
					      reaction,
					      reactionRateModel,
					      reactionRateModelParam,
					      rateDirection,
					      FOREIGN KEY(material) REFERENCES INTERFACES(Name), 
					      FOREIGN KEY(reaction) REFERENCES REACTIONS(Name),
					      FOREIGN KEY(reactionRateModel) REFERENCES MODELS(Name)
					      );
CREATE TABLE ELEMENTS (Name PRIMARY KEY, 
				     TexName, 
				     Description
				     );
INSERT INTO ELEMENTS VALUES('Cd','Cd','Cadmium');
INSERT INTO ELEMENTS VALUES('Te','Te','Tellurium');
INSERT INTO ELEMENTS VALUES('Cu','Cu','Copper');
INSERT INTO ELEMENTS VALUES('As','As','Arsenic');
INSERT INTO ELEMENTS VALUES('Cl','Cl','Chlorine');
INSERT INTO ELEMENTS VALUES('Sn','Sn','Stannum(Tin)');
INSERT INTO ELEMENTS VALUES('Zn','Zn','Zinc');
INSERT INTO ELEMENTS VALUES('S','S','Sulphur');
INSERT INTO ELEMENTS VALUES('O','O','Oxygen');
INSERT INTO ELEMENTS VALUES('G','G','Generic');
CREATE TABLE MODELS (Name PRIMARY KEY, 
				     Description,
				     nParam,
				     paramNameList,
				     paramUnits
				     );
INSERT INTO MODELS VALUES('fcGR','Free Carrier Generation Recombination Rate Model','0',NULL,NULL);
INSERT INTO MODELS VALUES('dlwA','Diffusion Limited With Attraction Rate Model','0',NULL,NULL);
INSERT INTO MODELS VALUES('fcCC','Free Carrier Capture Cross Section Rate Model','1','(capture_cross_section)','(cm^2)');
INSERT INTO MODELS VALUES('eDOS','Electron Free Carrier Density of States','0',NULL,NULL);
INSERT INTO MODELS VALUES('hDOS','Hole Free Carrier Density of States Density Model','0',NULL,NULL);
INSERT INTO MODELS VALUES('kDOS','Constant Density of States Lattice Site Density Model','0',NULL,NULL);
INSERT INTO MODELS VALUES('dDOS','Degenerate Density of States Lattice Site Density Model','0',NULL,NULL);
INSERT INTO MODELS VALUES('gDOS','General Density of States for Lattice Site Density','3','(atomic_degeneracy, site_degeneracy, density)','(Nil,Nil,cm^{-3})');
INSERT INTO MODELS VALUES('eGf','Electron Free Carrier Formation Energy','0',NULL,NULL);
INSERT INTO MODELS VALUES('hGf','Hole Free Carrier Formation Energy','0',NULL,NULL);
INSERT INTO MODELS VALUES('iGf','Intrinsic Host Species Formation Energy','0',NULL,NULL);
INSERT INTO MODELS VALUES('sGf','Standard Formation Energy','var','var','(eV)');
INSERT INTO MODELS VALUES('eD','Electron Free Carrier Diffusivity','0',NULL,NULL);
INSERT INTO MODELS VALUES('hD','Hole Free Carrier Diffusivity','0',NULL,NULL);
INSERT INTO MODELS VALUES('eLFMob','Low Field Electron Mobility','1','MU_n300','cm^2/Vs');
INSERT INTO MODELS VALUES('hLFMob','Low Field Hole Mobility','1','MU_p300','cm^2/Vs');
INSERT INTO MODELS VALUES('kD','Constant Diffusivity','1','(Diff Coeff)','(cm^2/s)');
INSERT INTO MODELS VALUES('bD','Barrier Diffusivity','2','(Diff Coeff,Barrier)','(cm^2/s,eV)');
INSERT INTO MODELS VALUES('vBG','Varshini Band Gap','3','(Eg0,alpha,beta)','(eV,eV/K,K)');
INSERT INTO MODELS VALUES('kBG','Constant Band Gap','1','(Eg)','(eV)');
CREATE TABLE VARMODELS (Name, 
				      Description,
				      nParam,
				      paramNameList,
				      paramUnits,
				      FOREIGN KEY(Name) REFERENCES MODELS(Name)
				      );
INSERT INTO VARMODELS VALUES('sGf','Standard Formation Energy','1','(G0)','(eV)');
INSERT INTO VARMODELS VALUES('sGf','Standard Formation Energy','2','(G0,(E1,vb))','(eV,(eV,Nil))');
INSERT INTO VARMODELS VALUES('sGf','Standard Formation Energy','3','(G0,(E1,vb),(E2,vb))','(eV,(eV,Nil),(eV,Nil))');
INSERT INTO VARMODELS VALUES('sGf','Standard Formation Energy','4','(G0,(E1,vb),(E2,vb),(E3,vb))','(eV,(eV,Nil),(eV,Nil),(eV,Nil))');
INSERT INTO VARMODELS VALUES('sGf','Standard Formation Energy','2','(G0,(E1,cb))','(eV,(eV,Nil))');
INSERT INTO VARMODELS VALUES('sGf','Standard Formation Energy','3','(G0,(E1,cb),(E2,cb))','(eV,(eV,Nil),(eV,Nil))');
INSERT INTO VARMODELS VALUES('sGf','Standard Formation Energy','4','(G0,(E1,cb),(E2,cb),(E3,cb))','(eV,(eV,Nil),(eV,Nil),(eV,Nil))');
CREATE TABLE REACTIONRATEMODELS (Name,
					   FOREIGN KEY(Name) REFERENCES MODELS(Name)
					   );
INSERT INTO REACTIONRATEMODELS VALUES('fcGR');
INSERT INTO REACTIONRATEMODELS VALUES('dlwA');
INSERT INTO REACTIONRATEMODELS VALUES('fcCC');
CREATE TABLE BGMODELS (Name,
					   FOREIGN KEY(Name) REFERENCES MODELS(Name)
					   );
INSERT INTO BGMODELS VALUES('kBG');
INSERT INTO BGMODELS VALUES('vBG');
CREATE TABLE SPECIESNSMODELS (Name,
					  FOREIGN KEY(Name) REFERENCES MODELS(Name)
					  );
INSERT INTO SPECIESNSMODELS VALUES('eDOS');
INSERT INTO SPECIESNSMODELS VALUES('hDOS');
INSERT INTO SPECIESNSMODELS VALUES('kDOS');
INSERT INTO SPECIESNSMODELS VALUES('dDOS');
INSERT INTO SPECIESNSMODELS VALUES('gDOS');
CREATE TABLE SPECIESGFMODELS (Name,
					    FOREIGN KEY(Name) REFERENCES MODELS(Name)
					    );
INSERT INTO SPECIESGFMODELS VALUES('eGf');
INSERT INTO SPECIESGFMODELS VALUES('hGf');
INSERT INTO SPECIESGFMODELS VALUES('sGf');
INSERT INTO SPECIESGFMODELS VALUES('iGf');
CREATE TABLE FREECARRIERMOBMODEL (Name,
					    FOREIGN KEY(Name) REFERENCES MODELS(Name)
						);
INSERT INTO FREECARRIERMOBMODEL VALUES('eLFMob');
INSERT INTO FREECARRIERMOBMODEL VALUES('hLFMob');
CREATE TABLE SPECIESDIFFMODELS (Name,
					    FOREIGN KEY(Name) REFERENCES MODELS(Name)
					    );
INSERT INTO SPECIESDIFFMODELS VALUES('eD');
INSERT INTO SPECIESDIFFMODELS VALUES('hD');
INSERT INTO SPECIESDIFFMODELS VALUES('kD');
INSERT INTO SPECIESDIFFMODELS VALUES('bD');
CREATE TABLE LATTICE_SITES (Name PRIMARY KEY, 
					  TexName, 
					  Description, 
					  Density
					  );
INSERT INTO LATTICE_SITES VALUES('A','A','Anion Site','Model:ConstantDensity');
INSERT INTO LATTICE_SITES VALUES('C','C','Cation Site','Model:ConstantDensity');
INSERT INTO LATTICE_SITES VALUES('VB','v','Valence Band Site','Model:EffectiveDensityofStatesForHoles');
INSERT INTO LATTICE_SITES VALUES('CB','c','Conduction Band Site','Model:EffectiveDensityofStatesForElectrons');
INSERT INTO LATTICE_SITES VALUES('M','M','Hexagonal Site','Model:ConstantDensity');
INSERT INTO LATTICE_SITES VALUES('i','i','Interstitial Site','Model:ConstantDensity');
INSERT INTO LATTICE_SITES VALUES('g','g','Generic Site','Model:ConstantDensity');
CREATE TABLE MATERIALS (Name STRING PRIMARY KEY, 
				      Cation, 
				      Anion,
				      "Lattice Density", 
				      "Electron Effective Mass", 
				      "Hole Effective Mass",
				      "Electron Affinity",
				      "Dielectric Permittivity",
				      "Radiative Rate Constant",
				      "Formation Energy",
				      "Band Gap Model",
				      "Band Gap Params",
				      "Electron Mobility Model",
				      "Electron Mobility Params",
				      "Hole Mobility Model",
				      "Hole Mobility Params",
				      FOREIGN KEY(Cation) REFERENCES ELEMENTS (Name),
				      FOREIGN KEY(Anion) REFERENCES ELEMENTS (Name),
				      FOREIGN KEY("Band Gap Model") REFERENCES MODELS (Name)
			              );
INSERT INTO MATERIALS VALUES('SnO','Sn','O',1.4687000000000000262e+22,0.09500000000000000111,0.83999999999999996891,4.4000000000000003552,11,9.9999999999999993945e-12,0.0,'vBG','(3.6,0,160)','eLFMob','(400)','hLFMob','(200)');
INSERT INTO MATERIALS VALUES('CdS','Cd','S',2.0920000000000002096e+22,0.089999999999999996669,0.59999999999999997779,4.4000000000000003552,11,9.9999999999999993945e-12,0.0,'vBG','(2.5,0,160)','eLFMob','(300)','hLFMob','(100)');
INSERT INTO MATERIALS VALUES('ZnTe','Zn','Te',1.7619999999999998952e+22,0.089999999999999996669,0.59999999999999997779,3.5299999999999998046,11,9.9999999999999993945e-12,0.0,'vBG','(2.25,0,160)','eLFMob','(400)','hLFMob','(200)');
INSERT INTO MATERIALS VALUES('CdTe','Cd','Te',1.4687000000000000262e+22,0.09500000000000000111,0.83999999999999996891,4.4000000000000003552,11,9.9999999999999993945e-12,-1.1699999999999999289,'vBG','(1.602,5e-4,160)','eLFMob','(400)','hLFMob','(200)');
CREATE TABLE INTERFACES (Name PRIMARY KEY,
				       mat1,
				       mat2,
				       prefer,
				       FOREIGN KEY(mat1) REFERENCES MATERIALS(Name),
				       FOREIGN KEY(mat2) REFERENCES MATERIALS(Name),
				       FOREIGN KEY(prefer) REFERENCES MATERIALS(Name)
				       );
INSERT INTO INTERFACES VALUES('CdTe/CdTe','CdTe','CdTe','CdTe');
INSERT INTO INTERFACES VALUES('CdTe/ZnTe','CdTe','ZnTe','CdTe');
INSERT INTO INTERFACES VALUES('ZnTe/CdTe','ZnTe','CdTe','CdTe');
INSERT INTO INTERFACES VALUES('ZnTe/ZnTe','ZnTe','ZnTe','ZnTe');
INSERT INTO INTERFACES VALUES('CdTe/CdS','CdTe','CdS','CdTe');
INSERT INTO INTERFACES VALUES('CdS/CdTe','CdS','CdTe','CdTe');
INSERT INTO INTERFACES VALUES('CdS/CdS','CdS','CdS','CdS');
INSERT INTO INTERFACES VALUES('CdS/SnO','CdS','SnO','CdS');
INSERT INTO INTERFACES VALUES('SnO/CdS','SnO','CdS','CdS');
INSERT INTO INTERFACES VALUES('SnO/SnO','SnO','SnO','SnO');
CREATE TABLE SPECIES (Name PRIMARY KEY, 
				    Charge,
				    Description,
				    elementscount
				    );
INSERT INTO SPECIES VALUES('h_{v}^{+}',1,'Holes in valence band',NULL);
INSERT INTO SPECIES VALUES('e_{c}^{-}',-1,'Electrons in conduction band',NULL);
INSERT INTO SPECIES VALUES('D_{g}^{+}',1,'Generic Donor at Generic site with +1 charge','(("G",1),)');
INSERT INTO SPECIES VALUES('D_{g}^{0}',0,'Generic Donor at Generic site with 0 charge','(("G",1),)');
INSERT INTO SPECIES VALUES('A_{g}^{-}',-1,'Generic Acceptor at Generic site with -1 charge','(("G",1),)');
INSERT INTO SPECIES VALUES('A_{g}^{0}',0,'Generic Acceptor at Generic site with 0 charge','(("G",1),)');
INSERT INTO SPECIES VALUES('V_{C}^{0}',0,'Vacancy at Cation site with 0 charge','(("Cd",-1),)');
INSERT INTO SPECIES VALUES('V_{C}^{-}',-1,'Vacancy at Cation site with -1 charge','(("Cd",-1),)');
INSERT INTO SPECIES VALUES('V_{C}^{2-}',-2,'Vacancy at Cation site with -2 charge','(("Cd",-1),)');
INSERT INTO SPECIES VALUES('V_{A}^{0}',0,'Vacancy at Anion site with 0 charge','(("Te",-1),)');
INSERT INTO SPECIES VALUES('V_{A}^{+}',1,'Vacancy at Anion site with +1 charge','(("Te",-1),)');
INSERT INTO SPECIES VALUES('V_{A}^{2+}',2,'Vacancy at Anion site with +2 charge','(("Te",-1),)');
INSERT INTO SPECIES VALUES('Te_{i}^{0}',0,'Tellurium interstitial with 0 charge','(("Te",1),)');
INSERT INTO SPECIES VALUES('Te_{i}^{+}',1,'Tellurium interstitial with +1 charge','(("Te",1),)');
INSERT INTO SPECIES VALUES('Te_{i}^{2+}',2,'Tellurium interstitial with +2 charge','(("Te",1),)');
INSERT INTO SPECIES VALUES('Cd_{i}^{0}',0,'Tellurium interstitial with 0 charge','(("Cd",1),)');
INSERT INTO SPECIES VALUES('Cd_{i}^{+}',1,'Tellurium interstitial with +1 charge','(("Cd",1),)');
INSERT INTO SPECIES VALUES('Cd_{i}^{2+}',2,'Tellurium interstitial with +2 charge','(("Cd",1),)');
INSERT INTO SPECIES VALUES('Te_{C}^{0}',0,'Tellurium antisite with 0 charge','(("Te",1),("Cd",-1),)');
INSERT INTO SPECIES VALUES('Te_{C}^{-}',-1,'Tellurium antisite with -1 charge','(("Te",1),("Cd",-1),)');
INSERT INTO SPECIES VALUES('Te_{C}^{2-}',-2,'Tellurium antisite with -2 charge','(("Te",1),("Cd",-1),)');
INSERT INTO SPECIES VALUES('Te_{C}^{+}',1,'Tellurium antisite with  1 charge','(("Te",1),("Cd",-1),)');
INSERT INTO SPECIES VALUES('Te_{C}^{2+}',2,'Tellurium antisite with  2 charge','(("Te",1),("Cd",-1),)');
INSERT INTO SPECIES VALUES('Cd_{C}^{0}',0,'Cadmium at Cation site with 0 charge','(("Cd",1),)');
INSERT INTO SPECIES VALUES('Te_{A}^{0}',0,'Tellurium at Anion site with 0 charge','(("Te",1),)');
CREATE TABLE MATERIALSPECIES (material , 
					    species, 
					    site,
					    NsModelName,
					    NsModelParam,
					    GfModelName,
					    GfModelParam,
					    DModelName,
					    DModelParam, 
					    FOREIGN KEY(material) REFERENCES MATERIALS(Name), 
					    FOREIGN KEY(species) REFERENCES SPECIES(Name), 
					    FOREIGN KEY(site) REFERENCES LATTICE_SITES(Name),
					    FOREIGN KEY(NsModelName) REFERENCES MODELS(Name),
					    FOREIGN KEY(GfModelName) REFERENCES MODELS(Name),
					    FOREIGN KEY(DModelName) REFERENCES MODELS(Name)
					    );
INSERT INTO MATERIALSPECIES VALUES('CdTe','e_{c}^{-}','CB','eDOS',NULL,'eGf',NULL,'eD',NULL);
INSERT INTO MATERIALSPECIES VALUES('ZnTe','e_{c}^{-}','CB','eDOS',NULL,'eGf',NULL,'eD',NULL);
INSERT INTO MATERIALSPECIES VALUES('CdS','e_{c}^{-}','CB','eDOS',NULL,'eGf',NULL,'eD',NULL);
INSERT INTO MATERIALSPECIES VALUES('SnO','e_{c}^{-}','CB','eDOS',NULL,'eGf',NULL,'eD',NULL);
INSERT INTO MATERIALSPECIES VALUES('CdTe','h_{v}^{+}','VB','hDOS',NULL,'hGf',NULL,'hD',NULL);
INSERT INTO MATERIALSPECIES VALUES('ZnTe','h_{v}^{+}','VB','hDOS',NULL,'hGf',NULL,'hD',NULL);
INSERT INTO MATERIALSPECIES VALUES('CdS','h_{v}^{+}','VB','hDOS',NULL,'hGf',NULL,'hD',NULL);
INSERT INTO MATERIALSPECIES VALUES('SnO','h_{v}^{+}','VB','hDOS',NULL,'hGf',NULL,'hD',NULL);
INSERT INTO MATERIALSPECIES VALUES('CdTe','D_{g}^{0}','C','kDOS',NULL,'sGf','(0,)','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('ZnTe','D_{g}^{0}','C','kDOS',NULL,'sGf','(0,)','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdS','D_{g}^{0}','C','kDOS',NULL,'sGf','(0,)','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('SnO','D_{g}^{0}','C','kDOS',NULL,'sGf','(0,)','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','D_{g}^{+}','C','kDOS',NULL,'sGf','(0,(0.1,-1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('ZnTe','D_{g}^{+}','C','kDOS',NULL,'sGf','(0,(0.1,-1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdS','D_{g}^{+}','C','kDOS',NULL,'sGf','(0,(0.1,-1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('SnO','D_{g}^{+}','C','kDOS',NULL,'sGf','(0,(0.1,-1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','A_{g}^{0}','A','kDOS',NULL,'sGf','(0,)','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('ZnTe','A_{g}^{0}','A','kDOS',NULL,'sGf','(0,)','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdS','A_{g}^{0}','A','kDOS',NULL,'sGf','(0,)','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('SnO','A_{g}^{0}','A','kDOS',NULL,'sGf','(0,)','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','A_{g}^{-}','A','kDOS',NULL,'sGf','(0,(0.1,1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('ZnTe','A_{g}^{-}','A','kDOS',NULL,'sGf','(0,(0.1,1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdS','A_{g}^{-}','A','kDOS',NULL,'sGf','(0,(0.1,1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('SnO','A_{g}^{-}','A','kDOS',NULL,'sGf','(0,(0.1,1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','V_{C}^{0}','C','kDOS',NULL,'sGf','(3.023,)','bD','(0.021,1.83)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','V_{C}^{-}','C','kDOS',NULL,'sGf','(3.023,(0.524,1))','bD','(0.021,1.83)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','V_{C}^{2-}','C','kDOS',NULL,'sGf','(3.023,(0.524,1),(0.204,1))','bD','(0.021,1.83)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','V_{A}^{0}','C','kDOS',NULL,'sGf','(3.006,)','bD','(0.021,1.42)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','V_{A}^{+}','C','kDOS',NULL,'sGf','(3.006,(0.42,-1))','bD','(0.021,1.7)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','V_{A}^{2+}','C','kDOS',NULL,'sGf','(3.006,(0.42,-1),(0.12,-1))','bD','(0.021,1.7)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Te_{i}^{0}','i','kDOS',NULL,'sGf','(1.824,)','bD','(0.007,0.09)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Te_{i}^{+}','i','kDOS',NULL,'sGf','(1.824,(0.2,1))','bD','(0.007,0.38)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Te_{i}^{2+}','i','kDOS',NULL,'sGf','(1.824,(0.2,1),(0.56,1))','bD','(0.007,0.38)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Cd_{i}^{0}','i','kDOS',NULL,'sGf','(2.023,)','bD','(0.0052,0.6)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Cd_{i}^{+}','i','kDOS',NULL,'sGf','(2.023,(0.21,-1))','bD','(0.00321,0.47)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Cd_{i}^{2+}','i','kDOS',NULL,'sGf','(2.023,(0.21,-1),(0.21,-1))','bD','(0.00321,0.47)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Te_{C}^{0}','C','kDOS',NULL,'sGf','(2.573,)','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Te_{C}^{+}','C','kDOS',NULL,'sGf','(2.573,(0.446,1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Te_{C}^{2+}','C','kDOS',NULL,'sGf','(2.573,(0.446,1),(0.546,1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Te_{C}^{-}','C','kDOS',NULL,'sGf','(2.573,(0.217,-1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Te_{C}^{2-}','C','kDOS',NULL,'sGf','(2.573,(0.217,-1),(0.217,-1))','kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Cd_{C}^{0}','C','kDOS',NULL,'iGf',NULL,'kD','(0)');
INSERT INTO MATERIALSPECIES VALUES('CdTe','Te_{A}^{0}','A','kDOS',NULL,'iGf',NULL,'kD','(0)');
CREATE TABLE INTERFACESPECIES (material , 
					    species, 
					    site,
					    NsModelName,
					    NsModelParam,
					    GfModelName,
					    GfModelParam,
					    DModelName,
					    DModelParam, 
					    FOREIGN KEY(material) REFERENCES INTERFACES(Name), 
					    FOREIGN KEY(species) REFERENCES SPECIES(Name), 
					    FOREIGN KEY(site) REFERENCES LATTICE_SITES(Name),
					    FOREIGN KEY(NsModelName) REFERENCES MODELS(Name),
					    FOREIGN KEY(GfModelName) REFERENCES MODELS(Name),
					    FOREIGN KEY(DModelName) REFERENCES MODELS(Name)
					    );
INSERT INTO INTERFACESPECIES VALUES('CdTe/ZnTe','D_{g}^{+}','C','kDOS',NULL,'sGf','(0,(0.2,1))','kD','(0)');
INSERT INTO INTERFACESPECIES VALUES('ZnTe/CdTe','D_{g}^{+}','C','kDOS',NULL,'sGf','(0,(0.2,1))','kD','(0)');
CREATE TABLE REACTIONS (Name PRIMARY KEY,
				      LHSspecies1,
				      LHSspecies2,
				      RHSspecies1,
				      RHSspecies2,
				      Description,
				      FOREIGN KEY(LHSspecies1) REFERENCES SPECIES(Name),
				      FOREIGN KEY(LHSspecies2) REFERENCES SPECIES(Name),
				      FOREIGN KEY(RHSspecies1) REFERENCES SPECIES(Name),
				      FOREIGN KEY(RHSspecies2) REFERENCES SPECIES(Name)
				      );
INSERT INTO REACTIONS VALUES('NULL <=> e_{c}^{-} + h_{v}^{+}',NULL,NULL,'e_{c}^{-}','h_{v}^{+}','Electron-Hole generation/recombination reaction');
INSERT INTO REACTIONS VALUES('D_{g}^{0} <=> D_{g}^{+} + e_{c}^{-}','D_{g}^{0}',NULL,'D_{g}^{+}','e_{c}^{-}','Generic Donor electron capture reaction');
INSERT INTO REACTIONS VALUES('D_{g}^{+} <=> D_{g}^{0} + h_{v}^{+}','D_{g}^{+}',NULL,'D_{g}^{0}','h_{v}^{+}','Generic Donor hole capture reaction');
INSERT INTO REACTIONS VALUES('A_{g}^{0} <=> A_{g}^{-} + h_{v}^{+}','A_{g}^{0}',NULL,'A_{g}^{-}','h_{v}^{+}','Generic Acceptor hole capture reaction');
INSERT INTO REACTIONS VALUES('A_{g}^{-} <=> A_{g}^{0} + e_{c}^{-}','A_{g}^{-}',NULL,'A_{g}^{0}','e_{c}^{-}','Generic Acceptor electron capture reaction');
INSERT INTO REACTIONS VALUES('V_{C}^{-}  <=> V_{C}^{0}  + e_{c}^{-}','V_{C}^{-}',NULL,'V_{C}^{0}','e_{c}^{-}','Vacancy at Cation Site Acceptor electron capture reaction');
INSERT INTO REACTIONS VALUES('V_{C}^{2-} <=> V_{C}^{-}  + e_{c}^{-}','V_{C}^{2-}',NULL,'V_{C}^{-}','e_{c}^{-}','Charged Vacancy at Cation Site Acceptor electron capture reaction');
INSERT INTO REACTIONS VALUES('V_{C}^{-}  <=> V_{C}^{2-} + h_{v}^{+}','V_{C}^{-}',NULL,'V_{C}^{2-}','h_{v}^{+}','Charged Vacancy at Cation Site Acceptor hole capture reaction');
INSERT INTO REACTIONS VALUES('V_{C}^{0}  <=> V_{C}^{-}  + h_{v}^{+}','V_{C}^{0}',NULL,'V_{C}^{-}','h_{v}^{+}','Charged Vacancy at Cation Site Acceptor hole capture reaction');
INSERT INTO REACTIONS VALUES('Te_{C}^{-}  <=> Te_{C}^{0}  + e_{c}^{-}','Te_{C}^{-}',NULL,'Te_{C}^{0}','e_{c}^{-}','Tellurium at Cation Site Acceptor electron capture reaction');
INSERT INTO REACTIONS VALUES('Te_{C}^{2-} <=> Te_{C}^{-}  + e_{c}^{-}','Te_{C}^{2-}',NULL,'Te_{C}^{-}','e_{c}^{-}','Charged Tellurium at Cation Site Acceptor electron capture reaction');
INSERT INTO REACTIONS VALUES('Te_{C}^{-}  <=> Te_{C}^{2-} + h_{v}^{+}','Te_{C}^{-}',NULL,'Te_{C}^{2-}','h_{v}^{+}','Charged Tellurium at Cation Site Acceptor hole capture reaction');
INSERT INTO REACTIONS VALUES('Te_{C}^{0}  <=> Te_{C}^{-}  + h_{v}^{+}','Te_{C}^{0}',NULL,'Te_{C}^{-}','h_{v}^{+}','Charged Tellurium at Cation Site Acceptor hole capture reaction');
INSERT INTO REACTIONS VALUES('Te_{C}^{0}  <=> Te_{C}^{+}  + e_{c}^{-}','Te_{C}^{0}',NULL,'Te_{C}^{+}','e_{c}^{-}','Charged Tellurium at Cation Site Donor electron capture reaction');
INSERT INTO REACTIONS VALUES('Te_{C}^{+}  <=> Te_{C}^{2+} + e_{c}^{-}','Te_{C}^{+}',NULL,'Te_{C}^{2+}','e_{c}^{-}','Charged Tellurium at Cation Site Donor electron capture reaction');
INSERT INTO REACTIONS VALUES('Te_{C}^{+}  <=> Te_{C}^{0}  + h_{v}^{+}','Te_{C}^{+}',NULL,'Te_{C}^{0}','h_{v}^{+}','Tellurium at Cation Site Donor hole capture reaction');
INSERT INTO REACTIONS VALUES('Te_{C}^{2+} <=> Te_{C}^{+}  + h_{v}^{+}','Te_{C}^{2+}',NULL,'Te_{C}^{+}','h_{v}^{+}','Charged Tellurium at Cation Site Donor hole capture reaction');
INSERT INTO REACTIONS VALUES('V_{A}^{0}  <=> V_{A}^{+}  + e_{c}^{-}','V_{A}^{0}',NULL,'V_{A}^{+}','e_{c}^{-}','Charged Vacancy at Anion Site Donor electron capture reaction');
INSERT INTO REACTIONS VALUES('V_{A}^{+}  <=> V_{A}^{2+} + e_{c}^{-}','V_{A}^{+}',NULL,'V_{A}^{2+}','e_{c}^{-}','Charged Vacancy at Anion Site Donor electron capture reaction');
INSERT INTO REACTIONS VALUES('V_{A}^{+}  <=> V_{A}^{0}  + h_{v}^{+}','V_{A}^{+}',NULL,'V_{A}^{0}','h_{v}^{+}','Vacancy at Anion Site Donor hole capture reaction');
INSERT INTO REACTIONS VALUES('V_{A}^{2+} <=> V_{A}^{+}  + h_{v}^{+}','V_{A}^{2+}',NULL,'V_{A}^{+}','h_{v}^{+}','Charged Vacancy at Anion Site Donor hole capture reaction');
INSERT INTO REACTIONS VALUES('Cd_{i}^{0}  <=> Cd_{i}^{+}  + e_{c}^{-}','Cd_{i}^{0}',NULL,'Cd_{i}^{+}','e_{c}^{-}','Charged Cadmium Intersitial at Interstitial Site Donor electron capture reaction');
INSERT INTO REACTIONS VALUES('Cd_{i}^{+}  <=> Cd_{i}^{2+} + e_{c}^{-}','Cd_{i}^{+}',NULL,'Cd_{i}^{2+}','e_{c}^{-}','Charged Cadmium Intersitial at Interstitial Site Donor electron capture reaction');
INSERT INTO REACTIONS VALUES('Cd_{i}^{+}  <=> Cd_{i}^{0}  + h_{v}^{+}','Cd_{i}^{+}',NULL,'Cd_{i}^{0}','h_{v}^{+}','Cadmium Intersitial at Interstitial Site Donor hole capture reaction');
INSERT INTO REACTIONS VALUES('Cd_{i}^{2+} <=> Cd_{i}^{+}  + h_{v}^{+}','Cd_{i}^{2+}',NULL,'Cd_{i}^{+}','h_{v}^{+}','Charged Cadmium Intersitial at Interstitial Site Donor hole capture reaction');
INSERT INTO REACTIONS VALUES('Te_{i}^{0}  <=> Te_{i}^{+}  + e_{c}^{-}','Te_{i}^{0}',NULL,'Te_{i}^{+}','e_{c}^{-}','Charged Tellurium Intersitial at Interstitial Site Donor electron capture reaction');
INSERT INTO REACTIONS VALUES('Te_{i}^{+}  <=> Te_{i}^{2+} + e_{c}^{-}','Te_{i}^{+}',NULL,'Te_{i}^{2+}','e_{c}^{-}','Charged Tellurium Intersitial at Interstitial Site Donor electron capture reaction');
INSERT INTO REACTIONS VALUES('Te_{i}^{+}  <=> Te_{i}^{0}  + h_{v}^{+}','Te_{i}^{+}',NULL,'Te_{i}^{0}','h_{v}^{+}','Tellurium Intersitial at Interstitial Site Donor hole capture reaction');
INSERT INTO REACTIONS VALUES('Te_{i}^{2+} <=> Te_{i}^{+}  + h_{v}^{+}','Te_{i}^{2+}',NULL,'Te_{i}^{+}','h_{v}^{+}','Charged Tellurium Intersitial at Interstitial Site Donor hole capture reaction');
INSERT INTO REACTIONS VALUES('Te_{A}^{0}  <=> V_{A}^{0}  + Te_{i}^{0}','Te_{A}^{0}',NULL,'V_{A}^{0}','Te_{i}^{0}','Frenkel pair formation reaction');
INSERT INTO REACTIONS VALUES('Cd_{C}^{0}  <=> V_{C}^{0}  + Cd_{i}^{0}','Cd_{C}^{0}',NULL,'V_{C}^{0}','Cd_{i}^{0}','Frenkel pair formation reaction');
INSERT INTO REACTIONS VALUES('Cd_{C}^{0}  <=> V_{C}^{2-} + Cd_{i}^{2+}','Cd_{C}^{0}',NULL,'V_{C}^{2-}','Cd_{i}^{2+}','Frenkel pair formation reaction');
INSERT INTO REACTIONS VALUES('Te_{C}^{0}  <=> V_{C}^{0}  + Te_{i}^{0}','Te_{C}^{0}',NULL,'V_{C}^{0}','Te_{i}^{0}','Tellurium Antisite defect formation reaction');
INSERT INTO REACTIONS VALUES('Te_{C}^{0}  <=> V_{C}^{2-} + Te_{i}^{2+}','Te_{C}^{0}',NULL,'V_{C}^{2-}','Te_{i}^{2+}','Tellurium Antisite defect formation reaction');
CREATE TABLE MATERIALREACTIONS (material,
					      reaction,
					      reactionRateModel,
					      reactionRateModelParam,
					      rateDirection,
					      FOREIGN KEY(material) REFERENCES MATERIALS(Name), 
					      FOREIGN KEY(reaction) REFERENCES REACTIONS(Name),
					      FOREIGN KEY(reactionRateModel) REFERENCES MODELS(Name)
					      );
INSERT INTO MATERIALREACTIONS VALUES('CdTe','NULL <=> e_{c}^{-} + h_{v}^{+}','fcGR',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('ZnTe','NULL <=> e_{c}^{-} + h_{v}^{+}','fcGR',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('CdS','NULL <=> e_{c}^{-} + h_{v}^{+}','fcGR',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('SnO','NULL <=> e_{c}^{-} + h_{v}^{+}','fcGR',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','D_{g}^{0} <=> D_{g}^{+} + e_{c}^{-}','dlwA',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('ZnTe','D_{g}^{0} <=> D_{g}^{+} + e_{c}^{-}','dlwA',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('CdS','D_{g}^{0} <=> D_{g}^{+} + e_{c}^{-}','dlwA',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('SnO','D_{g}^{0} <=> D_{g}^{+} + e_{c}^{-}','dlwA',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','D_{g}^{+} <=> D_{g}^{0} + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('ZnTe','D_{g}^{+} <=> D_{g}^{0} + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdS','D_{g}^{+} <=> D_{g}^{0} + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('SnO','D_{g}^{+} <=> D_{g}^{0} + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','A_{g}^{0} <=> A_{g}^{-} + h_{v}^{+}','dlwA',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('ZnTe','A_{g}^{0} <=> A_{g}^{-} + h_{v}^{+}','dlwA',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('CdS','A_{g}^{0} <=> A_{g}^{-} + h_{v}^{+}','dlwA',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('SnO','A_{g}^{0} <=> A_{g}^{-} + h_{v}^{+}','dlwA',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','A_{g}^{-} <=> A_{g}^{0} + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('ZnTe','A_{g}^{-} <=> A_{g}^{0} + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdS','A_{g}^{-} <=> A_{g}^{0} + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('SnO','A_{g}^{-} <=> A_{g}^{0} + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','V_{C}^{-}  <=> V_{C}^{0}  + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','V_{C}^{2-} <=> V_{C}^{-}  + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','V_{C}^{-}  <=> V_{C}^{2-} + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','V_{C}^{0}  <=> V_{C}^{-}  + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{C}^{-}  <=> Te_{C}^{0}  + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{C}^{2-} <=> Te_{C}^{-}  + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{C}^{-}  <=> Te_{C}^{2-} + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{C}^{0}  <=> Te_{C}^{-}  + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{C}^{0}  <=> Te_{C}^{+}  + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{C}^{+}  <=> Te_{C}^{2+} + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{C}^{+}  <=> Te_{C}^{0}  + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{C}^{2+} <=> Te_{C}^{+}  + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','V_{A}^{0}  <=> V_{A}^{+}  + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','V_{A}^{+}  <=> V_{A}^{2+} + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','V_{A}^{+}  <=> V_{A}^{0}  + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','V_{A}^{2+} <=> V_{A}^{+}  + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Cd_{i}^{0}  <=> Cd_{i}^{+}  + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Cd_{i}^{+}  <=> Cd_{i}^{2+} + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Cd_{i}^{+}  <=> Cd_{i}^{0}  + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Cd_{i}^{2+} <=> Cd_{i}^{+}  + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{i}^{0}  <=> Te_{i}^{+}  + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{i}^{+}  <=> Te_{i}^{2+} + e_{c}^{-}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{i}^{+}  <=> Te_{i}^{0}  + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{i}^{2+} <=> Te_{i}^{+}  + h_{v}^{+}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{A}^{0}  <=> V_{A}^{0}  + Te_{i}^{0}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Cd_{C}^{0}  <=> V_{C}^{0}  + Cd_{i}^{0}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Cd_{C}^{0}  <=> V_{C}^{2-} + Cd_{i}^{2+}','dlwA',NULL,'B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{C}^{0}  <=> V_{C}^{0}  + Te_{i}^{0}','fcCC','(1e-17)','B');
INSERT INTO MATERIALREACTIONS VALUES('CdTe','Te_{C}^{0}  <=> V_{C}^{2-} + Te_{i}^{2+}','dlwA',NULL,'B');
CREATE TABLE MECHANISMS (Name PRIMARY KEY,
				       Description
				       );
INSERT INTO MECHANISMS VALUES('Radiative','Radiative process mechanisms');
INSERT INTO MECHANISMS VALUES('Generic Donor','Generic Donor carrier capture mechanisms');
INSERT INTO MECHANISMS VALUES('Generic Acceptor','Generic Acceptor carrier capture mechanisms');
INSERT INTO MECHANISMS VALUES('Intrinsic','Intrinsic Defect Reaction mechanisms');
CREATE TABLE MATERIALMECHANISMREACTIONS (material,
						       mechanism,
						       reaction,
						       FOREIGN KEY(material) REFERENCES MATERIALS(Name),
						       FOREIGN KEY(mechanism) REFERENCES MECHANISMS(Name),
						       FOREIGN KEY(reaction) REFERENCES REACTIONS(Name)
						       );
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Radiative','NULL <=> e_{c}^{-} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('ZnTe','Radiative','NULL <=> e_{c}^{-} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdS','Radiative','NULL <=> e_{c}^{-} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('SnO','Radiative','NULL <=> e_{c}^{-} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Generic Donor','D_{g}^{0} <=> D_{g}^{+} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('ZnTe','Generic Donor','D_{g}^{0} <=> D_{g}^{+} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdS','Generic Donor','D_{g}^{0} <=> D_{g}^{+} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('SnO','Generic Donor','D_{g}^{0} <=> D_{g}^{+} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Generic Donor','D_{g}^{+} <=> D_{g}^{0} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('ZnTe','Generic Donor','D_{g}^{+} <=> D_{g}^{0} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdS','Generic Donor','D_{g}^{+} <=> D_{g}^{0} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('SnO','Generic Donor','D_{g}^{+} <=> D_{g}^{0} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Generic Acceptor','A_{g}^{0} <=> A_{g}^{-} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('ZnTe','Generic Acceptor','A_{g}^{0} <=> A_{g}^{-} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdS','Generic Acceptor','A_{g}^{0} <=> A_{g}^{-} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('SnO','Generic Acceptor','A_{g}^{0} <=> A_{g}^{-} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Generic Acceptor','A_{g}^{-} <=> A_{g}^{0} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('ZnTe','Generic Acceptor','A_{g}^{-} <=> A_{g}^{0} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdS','Generic Acceptor','A_{g}^{-} <=> A_{g}^{0} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('SnO','Generic Acceptor','A_{g}^{-} <=> A_{g}^{0} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','V_{C}^{-}  <=> V_{C}^{0}  + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','V_{C}^{2-} <=> V_{C}^{-}  + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','V_{C}^{-}  <=> V_{C}^{2-} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','V_{C}^{0}  <=> V_{C}^{-}  + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{C}^{-}  <=> Te_{C}^{0}  + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{C}^{2-} <=> Te_{C}^{-}  + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{C}^{-}  <=> Te_{C}^{2-} + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{C}^{0}  <=> Te_{C}^{-}  + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{C}^{0}  <=> Te_{C}^{+}  + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{C}^{+}  <=> Te_{C}^{2+} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{C}^{+}  <=> Te_{C}^{0}  + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{C}^{2+} <=> Te_{C}^{+}  + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','V_{A}^{0}  <=> V_{A}^{+}  + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','V_{A}^{+}  <=> V_{A}^{2+} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','V_{A}^{+}  <=> V_{A}^{0}  + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','V_{A}^{2+} <=> V_{A}^{+}  + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Cd_{i}^{0}  <=> Cd_{i}^{+}  + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Cd_{i}^{+}  <=> Cd_{i}^{2+} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Cd_{i}^{+}  <=> Cd_{i}^{0}  + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Cd_{i}^{2+} <=> Cd_{i}^{+}  + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{i}^{0}  <=> Te_{i}^{+}  + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{i}^{+}  <=> Te_{i}^{2+} + e_{c}^{-}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{i}^{+}  <=> Te_{i}^{0}  + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{i}^{2+} <=> Te_{i}^{+}  + h_{v}^{+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{A}^{0}  <=> V_{A}^{0}  + Te_{i}^{0}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Cd_{C}^{0}  <=> V_{C}^{0}  + Cd_{i}^{0}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Cd_{C}^{0}  <=> V_{C}^{2-} + Cd_{i}^{2+}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{C}^{0}  <=> V_{C}^{0}  + Te_{i}^{0}');
INSERT INTO MATERIALMECHANISMREACTIONS VALUES('CdTe','Intrinsic','Te_{C}^{0}  <=> V_{C}^{2-} + Te_{i}^{2+}');
CREATE TABLE INTERFACEMECHANISMREACTIONS (material,
						       mechanism,
						       reaction,
						       FOREIGN KEY(material) REFERENCES INTERFACES(Name),
						       FOREIGN KEY(mechanism) REFERENCES MECHANISMS(Name),
						       FOREIGN KEY(reaction) REFERENCES REACTIONS(Name)
						       );
COMMIT;
