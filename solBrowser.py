#! /usr/bin/env python
from pycdts.PVRD import PVRD

from pycdts.solutionBrowser import SolutionBrowser
from PyQt5.QtWidgets import (QApplication)
import sys

app = QApplication(sys.argv)
if 1:
#    fName = './Projects/test0D_cdte_sol.out'
#    fName = './Projects/test0D_cdte_CdRich_3_sol.out'
#    fName = './Projects/PN1D_1e18_sol.out'
#    fName = './Projects/Reservior/CdRich_lowTemp_sol.out'
#    fName = './Projects/Reservior/CdRich_highTemp_sol.out'
#    fName = './Projects/Reservior/CdRich_highTemp_V_Te_sol.out'
#    fName = './Projects/Reservior/CdRich_lowTemp_long_sol.out'
#    fName = './Projects/PN2D_1_sol.out'
#    fName = './Projects/20190721/intrinsic_CdRich_800K_b_sol.out'
#    fName = './Projects/20190722/CdRich_800K_ioninzation_fccc_noRad_forOrigin_sol.out'
#    fName = '../../Dissertation/Simulations/InitialConditions/CdTe_0300K_mid_sol.out'
    fName = '../../Dissertation/Simulations/InitialConditions/CdTe_1200K_300K_high_sol.out'
#    fName = './Projects/20190724/CdRich_800K_initialConditions_sol.out'
    sb = SolutionBrowser(fName,None,None)
    sb.show()
else:
    ex=PVRD()
    ex.startApplication()

sys.exit(app.exec_())
